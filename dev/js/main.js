$(document).ready(function() {

  $('.hamburger').click(function() {
    $(this).toggleClass('active');
    $('#top-nav').stop('top-nav', false, true).slideToggle();
    
    $('.search-toggle').removeClass('active');
    $('#top-search').stop('top-search', false, true).slideUp();
  });

  $('.search-toggle').click(function() {
    $(this).toggleClass('active');
    $('#top-search').stop('top-search', false, true).slideToggle();

    $('.hamburger').removeClass('active');
    $('#top-nav').stop('top-nav', false, true).slideUp();
  });


  $('.list-scroll').mCustomScrollbar({
    theme: 'dark'
  });

  /* city */
  $('.city__current, .city .dropdown-bg').click(function() {
    $('.city').toggleClass('active');
    $('.city__dropdown').stop('city', false, true).slideToggle();
    $('.city .dropdown-bg').stop('dropdown-bg', false, true).fadeToggle();
    if($('.city').hasClass('active')){
      $('.city .search input').focus();
    }
    return false;
  });

  $('.city .search input').on('input',function(){
    var _that = $(this);
    $('.city__notfound').hide();
    $('.city .list-scroll li').addClass('is-show').not(function(index){
      return $(this).find('a').text().toLowerCase().indexOf(_that.val().toLowerCase()) != -1
    }).removeClass('is-show');
    if(!$('.city .list-scroll li').hasClass('is-show')){
      $('.city__notfound').show();
    }
  });

  $('.city .search form').submit(function(){
    return false;
  });

  /* end city */



  var timerSearch; 
  $('#top-search input').on('input', function() {
    var _this = $(this);
    timerSearch = setTimeout( function(){
      if(_this.val() != ""){
        _this.closest('.search').find('.search__dropdown').stop('search-dropdown', false, true).slideDown();
      }else{
        _this.closest('.search').find('.search__dropdown').stop('search-dropdown', false, true).slideUp();
      }
    }, 500)
  });

  $('#top-search input').on('blur', function() {
    var _this = $(this);
    clearInterval(timerSearch);
    _this.closest('.search').find('.search__dropdown').stop('search-dropdown', false, true).slideUp();
    
  });

  $('.js-popup-call').magnificPopup({
    closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="icon icon_call-close"></i></button>'
  });

  $.extend($.validator.messages, {
    required: 'Поля не заполнены или заполнены некорректно',
    remote: 'Поля не заполнены или заполнены некорректно',
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Не менее {0} символов."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
  });

  var validate = {
    rules:{
      name: {
        required: true,
        minlength: 3
      },
      phone:{
        required: true
      }
    },
    /*messages:{
      name: 'Поля не заполнены или заполнены некорректно',
      phone: 'Поля не заполнены или заполнены некорректно'
    },*/
    //errorElement: 'span',
    errorPlacement: function(error, element) {
      return;
    },
    submitHandler: function(){
      var that = this;
      /*$.ajax({
        type: 'POST',
        url: 'send.php',
        data: $(that.currentForm).serialize(),
        success: function(msg){*/
          $(that.currentForm).trigger('reset');
          $.magnificPopup.open({
            items:{
              src: "#popup-call-success"
            },
            closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i class="icon icon_call-close"></i></button>'
          });
          setTimeout($.magnificPopup.close, 5000);
        /*}
      });*/
    }
  };
  $("#popup-call form").validate(validate);

  var validate2 = validate;
  validate2.errorPlacement = null;
  validate2.errorElement = 'span';
  validate2.messages = {
    required: 'Поля не заполнены или заполнены некорректно',
  };

  validate2.submitHandler = function(){
      var that = this;
      /*$.ajax({
        type: 'POST',
        url: 'send.php',
        data: $(that.currentForm).serialize(),
        success: function(msg){*/
          $(that.currentForm).trigger('reset');
          $.magnificPopup.open({
            items:{
              src: "#popup-order-success"
            }
          });
          setTimeout($.magnificPopup.close, 5000);
        /*}
      });*/
    };

  $("#wholesale-form form").validate(validate2);


  $('.accordion__head').click(function() {
    if($(this).parent().hasClass('active')){
      $(this).parent().removeClass('active');
      $(this).next().stop('accordion', false, true).slideUp();
    }else{
      $(this).parent().siblings('.active').find('.accordion__body').stop('accordion', false, true).slideUp();
      $(this).parent().siblings('.active').removeClass('active');
      $(this).parent().addClass('active');
      $(this).next().stop('accordion', false, true).slideDown();
    }
    return false;
  });


  var mainSwiper = new Swiper ('.main-slider__slides.swiper-container', {
    loop: true,
    speed: 700,
    effect: 'slide',
    autoHeight: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.main-slider .slider-nav_next',
      prevEl: '.main-slider .slider-nav_prev',
    },
  });




  $(".js-popup-video").magnificPopup({
    type: 'iframe',
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
      },
    }
  });


  $('.checkout-item__remove').click(function() {
    $(this).closest('.checkout-item').remove();
  });

  /* counter */
  function counterChange(num, $counter) {
    var $field = $counter.find('.counter__field')
    $field.val(parseInt($field.val(),10) + num);
    if($field.val() < 1){
      $field.val(1);
    }
  }
  $('.counter__plus').click(function() {
    counterChange(1, $(this).closest('.counter'))
  });
  $('.counter__minus').click(function() {
    counterChange(-1, $(this).closest('.counter'))
  });
  /* end counter */


  /* select */
  $('.select__current').click(function(e) {
    e.stopPropagation();
    var $select = $(this).closest('.select');
    if(!$select.hasClass('active')){
      $('.select.active').find('.select__dropdown').stop().slideUp();
      $('.select.active').removeClass('active');
      $('.mobile-category.active').find('.mobile-category__dropdown').stop().slideUp();
      $('.mobile-category.active').removeClass('active');
    }
    if($select.find('.select__dropdown').outerWidth() + $select.offset().left > $(document).width()){
      $select.find('.select__dropdown').css({
        left: 'auto',
        right: 0
      });
    }else{
      $select.find('.select__dropdown').css({
        left: 0,
        right: 'auto'
      });
    }
    $select.find('.select__dropdown').stop().slideToggle();
    $select.toggleClass('active');
  });
  $(document).click(function(e) {
    if($(e.target).closest('.select.active').length <= 0){
      $('.select.active').find('.select__dropdown').stop().slideUp();
      $('.select.active').removeClass('active');
    }
  });
  $('.select__all').click(function() {
    var $select = $(this).closest('.select');
    $select.find('.select__current').text($select.data('placeholder'));
    $select.find('.select__dropdown').stop().slideToggle();
    $select.toggleClass('active');
    $(this).closest('.select').find('.select__dropdown li').removeClass('active');
  });
  $('.select__dropdown li').click(function() {
    var $select = $(this).closest('.select');
    $select.find('input').val($(this).data('value'));
    $select.find('.select__current').text($(this).text());
    $select.find('.select__dropdown').stop().slideToggle();
    $select.toggleClass('active');
    $(this).closest('.select').find('.select__dropdown li').removeClass('active');
    $(this).addClass('active');
  });
  /* end select */


  $('.view-type input').change(function() {
    $('.catalog').toggleClass('view-list');
  });

  /* mobile category */
  $('.mobile-category__current').click(function(e) {
    e.stopPropagation();
    var $mCategory = $(this).closest('.mobile-category');
    if(!$mCategory.hasClass('active')){
      $('.mobile-category.active').find('.mobile-category__dropdown').stop().slideUp();
      $('.mobile-category.active').removeClass('active');
      $('.select.active').find('.select__dropdown').stop().slideUp();
      $('.select.active').removeClass('active');
    }
    $mCategory.find('.mobile-category__dropdown').stop().slideToggle();
    $mCategory.toggleClass('active');
  });

  $(document).click(function(e) {
    if($(e.target).closest('.mobile-category.active').length <= 0){
      $('.mobile-category.active').find('.mobile-category__dropdown').stop().slideUp();
      $('.mobile-category.active').removeClass('active');
    }
  });
  /* end mobile category */


  /* product tab */
  $('.product__tab a').click(function() {
    if($(this).parent().hasClass('active')) return false;

    $(this).parent().siblings().removeClass('active');
    $('.product__tab-item.active').removeClass('active');

    $(this).parent().addClass('active');
    $($(this).attr('href')).addClass('active');

    return false;
  });
  /* end product tab */


  /* product slider */
  if($('.product-slider').length > 0){
    var productThumbSwiper = new Swiper ('.product-slider__thumb .swiper-container', {
      loop: true,
      loopedSlides: 5,
      speed: 700,
      effect: 'slide',
      direction: 'vertical',
      slidesPerView: 3,
      touchRatio: 0.2,
      slideToClickedSlide: true,
      navigation: {
        nextEl: '.product-slider__slide-thumb-button_next',
        prevEl: '.product-slider__slide-thumb-button_prev',
      },
    });
    var productSwiper = new Swiper ('.product-slider__slides .swiper-container', {
      loop: true,
      loopedSlides: 5,
      speed: 700,
      effect: 'slide',
      autoHeight: true,
    });
    productSwiper.controller.control = productThumbSwiper;
    productThumbSwiper.controller.control = productSwiper;
  }

  /* end product slider */


  /* product-card-slide */
  var similarSwiper = new Swiper ('#similar .product-card-slide__slides .swiper-container', {
    speed: 700,
    effect: 'slide',
    slidesPerView: 4,
    spaceBetween: -1,
    navigation: {
      nextEl: '#similar .product-card-slide-nav_next',
      prevEl: '#similar .product-card-slide-nav_prev',
    },
    breakpoints: {
      500: {
        slidesPerView: 1,
        slidesPerColumn: 1
      },
      660: {
        slidesPerView: 2,
        slidesPerColumn: 1
      },
      980: {
        slidesPerView: 3,
      }
    },
  });

  var breakpoint = window.matchMedia( '(max-width: 768px)' );
  var hitSwiper;

  var breakpointChecker = function() {
    if ( breakpoint.matches === true ) {
      if ( hitSwiper !== undefined ) hitSwiper.destroy( true, true );
        return;
      } else if ( breakpoint.matches === false ) {
        return enableSwiper();
      }
  };

  var enableSwiper = function() {
    hitSwiper = new Swiper ('#hit .product-card-slide__slides .swiper-container', {
      speed: 700,
      effect: 'slide',
      slidesPerView: 4,
      spaceBetween: -1,
      slidesPerColumn: $('#hit .product-card-slide__slides .swiper-container').data('row'),
      navigation: {
        nextEl: '#hit .product-card-slide-nav_next',
        prevEl: '#hit .product-card-slide-nav_prev',
      },
      breakpoints: {
        500: {
          slidesPerView: 1
        },
        660: {
          slidesPerView: 2
        },
        980: {
          slidesPerView: 3,
        }
      },
    });

  };

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();



});