var styleMap = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
];

function infoMap(data){

  return '<div class="info-map">'+
        '<span class="info-map__city">'+ data.city+
        ',</span> '+
        '<span class="info-map__address">'+ data.address+
        '</span> <br>'+
        '<span class="info-map__phone">'+ data.phone+
        '</span>'+
        '<div class="info-map__worktime">'+
          '<span>Работаем: </span>'+
          '<span class="info-map__worktime-list">'+data.worktime+'</span>'+
        '</div>'
      '</div>';
}

/*function mapAddress(data) {
  return '<div class="map-address" id="map-address">'+
            '<div class="map-address__city">'++
            '</div><a class="map-address__phone" href="tel:'++'">'++'</a>'+
            '<div class="map-address__address">Ул. Красноармейская, 79/2
            </div>
            <div class="map-address__worktime"><span>Работаем: </span><span class="map-address__worktime-list">Пн — Пт : 10:00 — 20:00 <br>Сб : 10:00 — 16:00<br>Вс : 10:00 — 14:00</span>
            </div>
          '</div>'
}*/
var map;
function initMap() {

  map = new google.maps.Map(document.getElementById('map-main'), {
    zoom: 17,
    center: window.appmap[0].position,
    styles: styleMap
  });

  var image = {
    url: '/images/map-marker.png',
    size: new google.maps.Size(39, 49),
  };

   var infobubble = new InfoBubble({
    content: '',
    shadowStyle: 1,
    padding: 0,
    borderRadius: 0,
    borderWidth: 5,
    borderColor: '#fff',
    disableAutoPan: true,
    backgroundClassName: 'infobubble',
    arrowPosition: 20,
    arrowSize: 20,
    arrowStyle: 2,
    closeSrc: '/images/close.png'
  });


  for(var i = 0; i < window.appmap.length; i++){
    for(var j = 0; j < window.appmap[i].market.length; j++){
      window.appmap[i].market[j].marker = new google.maps.Marker({
        position: window.appmap[i].market[j].position,
        map: map,
        icon: image
      });
      window.appmap[i].market[j].city = window.appmap[i].city;
      window.appmap[i].market[j].marker.html = infoMap(window.appmap[i].market[j])
      window.appmap[i].market[j].marker.addListener('click', function() {
        infobubble.setContent(this.html);
        infobubble.open(map, this);
      });
    }
  }

}



function initInnerMap() {
  var uluru = $('#map-inner').data('mapposition');
  var map = new google.maps.Map(document.getElementById('map-inner'), {
    zoom: 17,
    center: uluru,
    styles: styleMap
  });
  var image = {
    url: '/images/map-marker.png',
    size: new google.maps.Size(39, 49),
  };

  var infobubble = new InfoBubble({
    content: '',
    shadowStyle: 1,
    padding: 0,
    borderRadius: 0,
    borderWidth: 5,
    borderColor: '#fff',
    disableAutoPan: true,
    backgroundClassName: 'infobubble',
    arrowPosition: 20,
    arrowSize: 20,
    arrowStyle: 2,
    closeSrc: '/images/close.png'
  });

  var markers = $('#map-inner').data('info');
  for(var i = 0; i < markers.length; i++){
    markers[i].marker = new google.maps.Marker({
      position: markers[i].position,
      map: map,
      icon: image
    });
    markers[i].marker.html = infoMap(markers[i].data)
    markers[i].marker.addListener('click', function() {
      infobubble.setContent(this.html);
      infobubble.open(map, this);
    });

  }
}


$(document).ready(function() {

  $('#map-search input').on('input', function() {
    var _this = $(this);
    _this.removeData('city-id');
    timerSearch = setTimeout( function(){
      $('#map-search .list-scroll li').addClass('is-show').not(function(index){
        return $(this).text().toLowerCase().indexOf(_this.val().toLowerCase()) != -1
      }).removeClass('is-show');
      if(_this.val() != ""){
        if($('#map-search .list-scroll li').hasClass('is-show')){
          _this.closest('.search').find('.search__dropdown').stop('search-map-dropdown', false, true).slideDown();
        }
      }else{
        _this.closest('.search').find('.search__dropdown').stop('search-map-dropdown', false, true).slideUp();
      }
    }, 500)
  });

  $('#map-search input').on('blur', function() {
    var _this = $(this);
    clearInterval(timerSearch);
    _this.closest('.search').find('.search__dropdown').stop('search-dropdown', false, true).slideUp();
  });

  $('#map-search .list-scroll li').click(function() {
    $('#map-search input').val($(this).text());
    $('#map-search input').data('city-id', $(this).data('city-id'));
  });

  $('#map-search form').submit(function() {
    $('.map-notfound').hide();
    var cityId = $(this).find('input').data('city-id');
    if(typeof cityId == "undefined"){
      for(var i = 0; i < window.appmap.length; i++){
        if(window.appmap[i].city == $(this).find('input').val()){
          cityId = window.appmap[i].id;
          break;
        }
      }
      if(typeof cityId == "undefined"){
        cityId = 0;
        $('.map-notfound').show();
      }
    }
    $('[data-city-id="'+cityId+'"]').siblings().removeClass('active');
    $('[data-city-id="'+cityId+'"]').addClass('active');
    var position;
    for(var i = 0; i < window.appmap.length; i++){
      if(window.appmap[i].id == cityId){
        position = window.appmap[i].position;
        break;
      }
    }
    map.setCenter(new google.maps.LatLng( position.lat, position.lng ));
    return false;
  });


});